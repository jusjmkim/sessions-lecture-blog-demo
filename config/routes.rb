Rails.application.routes.draw do
  resources :users
  get '/login', to: 'sessions#login'
  post '/login', to: 'sessions#login_user'
  delete '/logout', to: 'sessions#logout'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
